package com.peng.service;

import com.peng.po.User;

/**
 * @author Purely
 * @version 1.8
 * @date 2020/7/21 15:44
 */
public interface UserService {

    User checkUser(String username, String password);

}

