package com.peng.service;

import com.peng.po.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Purely
 * @date 2020/8/1 16:11
 */
public interface CommentService {
    List<Comment> listCommentByBlogId(Long blogId);

    Comment saveComment(Comment comment);

    Page<Comment> listComment(Pageable pageable);
    void deleteComment(Long id);
}
