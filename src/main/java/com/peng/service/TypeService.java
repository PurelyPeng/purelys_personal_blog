package com.peng.service;

import com.peng.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Purely
 * @date 2020/7/21 17:48
 */

//分类管理
public interface TypeService {

    //新增
    Type saveType(Type type);
    //查询
    Type getType(Long id);
    Type getTypeByName(String name);
    List<Type> listType();
    //分页查询
    Page<Type> listType(Pageable pageable);
    List<Type> listTypeTop(Integer size);
    //修改
    Type updateType(Long id,Type type);
    //删除
    void deleteType(Long id);
}
