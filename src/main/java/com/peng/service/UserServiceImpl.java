package com.peng.service;

import com.peng.dao.UserRepository;
import com.peng.po.User;
import com.peng.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Purely
 * @version 1.8
 * @date 2020/7/21 15:47
 */

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;


    @Override
    public User checkUser(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username, MD5Utils.code(password));
        return user;
    }
}

