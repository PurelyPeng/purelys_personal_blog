package com.peng.dao;

import com.peng.po.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Purely
 * @version 1.8
 * @date 2020/7/21 15:49
 */
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsernameAndPassword(String username, String password);
}

