package com.peng.dao;

import com.peng.po.Tag;
import com.peng.po.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Purely
 * @date 2020/7/23 17:19
 */
public interface TagRepository extends JpaRepository<Tag,Long> {

    Tag findByName(String name);
    @Query("select t from Tag t")
    List<Tag> findTop(Pageable pageable);
}
