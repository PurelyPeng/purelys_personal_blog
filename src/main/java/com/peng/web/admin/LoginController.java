package com.peng.web.admin;

import com.peng.po.User;
import com.peng.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author Purely
 * @version 1.8
 * @date 2020/7/21 15:58
 */

@Controller
@RequestMapping("/admin")//设置全局路径
public class LoginController {

    @Autowired
    private UserService userService;
    //跳转登录页的方法
    @GetMapping
    public String loginPage(){

        return "admin/login";
    }

    //登录方法
    @PostMapping("login")
    public String login(@RequestParam String username,
                        @RequestParam String password,
                        HttpSession session,
                        RedirectAttributes attributes){

        User user = userService.checkUser( username,  password);
        if (user != null){
            user.setPassword(null);//防止密码跑在前面
            //将用户信息保存到session中
            session.setAttribute("user",user);
            return "admin/index";
        }else {
            attributes.addFlashAttribute("message","用户名和密码错误");//将错误信息保存到message
            return "redirect:/admin";//登录失败就重定向到登录页
        }
    }

    //注销
    @GetMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute("user");//删除session的用户信息
        return "redirect:/admin";
    }
}

