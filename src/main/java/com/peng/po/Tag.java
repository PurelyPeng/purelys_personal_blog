package com.peng.po;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Purely
 * @version 1.8
 * @date 2020/7/20 16:52
 */

@Entity
@Table(name = "tag")


public class Tag {

    @Id
    @GeneratedValue//生成策略
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "tags")
    private List<Blog> blogs = new ArrayList<>();

    public Tag() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

