# purely的个人博客

#### 介绍
参考小而美博客  

[教程视频地址：https://www.bilibili.com/video/BV13t411T72J/?p=4](https://www.bilibili.com/video/BV13t411T72J/?p=4)

#### 博客部分演示

##### 1.首页

![](https://images.cnblogs.com/cnblogs_com/PurelyPeng/1820328/o_200803141846QQ%E6%B5%8F%E8%A7%88%E5%99%A8%E6%88%AA%E5%9B%BE20200803221609.png)

##### 2.分类

![](https://images.cnblogs.com/cnblogs_com/PurelyPeng/1820328/o_200803141938QQ%E6%B5%8F%E8%A7%88%E5%99%A8%E6%88%AA%E5%9B%BE20200803221917.png)

##### 3.归档

![](https://images.cnblogs.com/cnblogs_com/PurelyPeng/1820328/o_200803142022QQ%E6%B5%8F%E8%A7%88%E5%99%A8%E6%88%AA%E5%9B%BE20200803222001.png)

##### 4.关于我

![](https://images.cnblogs.com/cnblogs_com/PurelyPeng/1820328/o_200803142101QQ%E6%B5%8F%E8%A7%88%E5%99%A8%E6%88%AA%E5%9B%BE20200803222040.png)

##### 5.后台管理

![](https://images.cnblogs.com/cnblogs_com/PurelyPeng/1820328/o_200803142601QQ%E6%B5%8F%E8%A7%88%E5%99%A8%E6%88%AA%E5%9B%BE20200803222535.png)


#### 安装教程

1.  创建跟配置文件一样的数据库名
2.  更改数据库配置 默认数据库名pblog  用户名root 密码123456
3.  只需要创建数据库，数据库表运行项目后会自己创建
4.  默认后台登录用户名为admin  密码123456

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
